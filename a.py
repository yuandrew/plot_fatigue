# %%
import plotly.express as px
from sklearn.decomposition import PCA
from sklearn import datasets
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import pandas as pd


# import some data to play with
iris = datasets.load_iris()
X = iris.data[:, :2]  # we only take the first two features.
Y = iris.target

x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5

plt.figure(2, figsize=(8, 6))
plt.clf()
# Plot the training points

#plt.scatter(X[:, 0], X[:, 1], c=Y, cmap=plt.cm.Paired)
#plt.xlabel('Sepal length')
#plt.ylabel('Sepal width')

#plt.xlim(x_min, x_max)
#plt.ylim(y_min, y_max)
# plt.xticks(())
# plt.yticks(())
print(type(iris))
data = pd.DataFrame(iris['data'])
y = pd.DataFrame(iris['target'])
aa = pd.concat([data, y], axis=1)
aa.columns = ['x1', 'x2', 'x3', 'x4', 'y']
print(aa)

# %%
fig = px.box(aa, x='y', y='x1', notched=True)
fig.show()


# %%
data = dict(
    number=[39, 27.4, 20.6, 11, 2],
    stage=["Website visit", "Downloads", "Potential customers", "Requested price", "Invoice sent"])
fig = px.funnel(data, x='number', y='stage')
fig.show()


# %%

app.layout = html.Div([
    html.H2(children='box plot'),
    dcc.Graph(
        id='box',
        figure=fig
    )
])

#%%

import tensorflow as tf
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Conv1D
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPool1D
from tensorflow.keras.layers import MaxPool2D
from tensorflow.keras.layers import Flatten
from tensorflow.keras.models import Sequential


#%%
model = Sequential()
model.add(Conv1D(filters=f_n, kernel_size=f_s, padding="same", activation="relu", input_shape=(sxy, sz)))
model.add(Conv1D(filters=f_n, kernel_size=f_s, padding="same", activation="relu"))
model.add(MaxPool1D(pool_size=2, strides=2, padding='valid'))
model.add(Dropout(dr))
model.add(Flatten())
model.add(Dense(units=l1n, activation='relu'))
model.add(Dropout(dr))
model.add(Dense(units=l2n, activation='softmax'))
model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['sparse_categorical_accuracy'])
model.summary()