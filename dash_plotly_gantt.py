# %%
import plotly.figure_factory as ff
import numpy as np
import pandas as pd
import random
import plotly_express as px
import plotly.graph_objects as go


id = pd.DataFrame(np.repeat('A', 24))
time = pd.DataFrame(range(1, 25))
fatigue = pd.DataFrame([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                       0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1])
fatigue2 = pd.DataFrame([0, 0, 0, 0, 0, 0, 0, 1, 1, 1,
                        0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1])
fatigue3 = pd.DataFrame([0, 0, 0, 1, 1, 0, 0, 1, 1, 1,
                        1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0])

p1 = pd.concat([id, time, fatigue], axis=1)
p2 = pd.concat([pd.DataFrame(np.repeat('B', 24)), time, fatigue2], axis=1)
p3 = pd.concat([pd.DataFrame(np.repeat('C', 24)), time, fatigue3], axis=1)
da = pd.concat([p1, p2, p3])
da.columns = ['id', 'time', 'fatigue']


for i in range(0, len(da)):
    if(da['fatigue'].iloc[i] == 1):
        da['fatigue'].iloc[i] = 'warning'
    else:
        da['fatigue'].iloc[i] = 'normal'


all = pd.DataFrame([])
for i in ['A', 'B', 'C']:
    did = i
    df = da[da.id == did]
    count = 0
    iid = pd.DataFrame([did])
    cc = pd.DataFrame([df.fatigue[0]])
    start = pd.DataFrame([1])
    end = pd.DataFrame([])

    while True:
        if count == (len(df.time)-2):
            if df.fatigue[count] == df.fatigue[count+1]:
                end = end.append([df.time[count+1]])

            elif df.fatigue[count] != df.fatigue[count+1]:
                end = end.append([df.time[count]])

                end = end.append([df.time[count+1]])
                iid = iid.append([did])
                cc = cc.append([df.fatigue[23]])
                start = start.append([df.time[count+1]])
            break

        if df.fatigue[count] != df.fatigue[count+1]:

            end = end.append([df.time[count]])

            iid = iid.append([did])
            cc = cc.append([df.fatigue[count+1]])
            start = start.append([df.time[count+1]])
            count = count+1
            continue

        if df.fatigue[count] == df.fatigue[count+1]:

            count = count+1
            continue

    da_gantt = pd.concat([iid, cc, start-0.5, end+0.5], axis=1)
    all = all.append([da_gantt])


da_gantt = pd.concat([iid, cc, start-0.5, end+0.5], axis=1)
all.columns = ['Task', 'fatigue', 'Start', 'Finish']

colors = {
    'warning': 'rgb(255,0,0)',
    "normal": 'rgb(0,200,10)'
}

gif = ff.create_gantt(all, index_col='fatigue', colors=colors,
                      group_tasks=True, show_colorbar=True)
gif

# %%
