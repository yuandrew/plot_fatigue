import math

import dash
import dash_core_components as dcc
import dash_html_components as html
import numpy as np
import pandas as pd
import plotly.graph_objs as go
import plotly_express as px
from dash.dependencies import Input, Output

app = dash.Dash()


id = pd.DataFrame(np.repeat('A', 24))
time = pd.DataFrame(range(1, 25))

fatigue = pd.DataFrame([0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1])
fatigue2 = pd.DataFrame([0, 0, 0, 0, 0, 0, 0, 1, 1, 1,
                        0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1])
fatigue3 = pd.DataFrame([0, 0, 0, 1, 1, 0, 0, 1, 1, 1,
                        1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0])


p1 = pd.concat([id, time, fatigue], axis=1)
p2 = pd.concat([pd.DataFrame(np.repeat('B', 24)), time, fatigue2], axis=1)
p3 = pd.concat([pd.DataFrame(np.repeat('C', 24)), time, fatigue3], axis=1)
da = pd.concat([p1, p2, p3])
da.columns = ['Driver ID', 'time', 'fatigue']
p1.columns = ['Driver ID', 'time', 'fatigue']
p2.columns = ['Driver ID', 'time', 'fatigue']
p3.columns = ['Driver ID', 'time', 'fatigue']


for j in [p1, p2, p3, da]:
    for i in range(0, len(j)):
        if(j['fatigue'].iloc[i] == 1):
            j['fatigue'].iloc[i] = 'warnimg'
        else:
            j['fatigue'].iloc[i] = 'normal'

'''
gifall = px.scatter(da, x='Driver ID', y='time', color='fatigue', range_y=[
    0, 25], opacity=.8)

gifa = px.scatter(p1, x='Driver ID', y='time', color='fatigue', range_y=[
                  0, 25], opacity=.8)
gifb = px.scatter(p2, x='Driver ID', y='time', color='fatigue', range_y=[
                  0, 25], opacity=.8)
gifc = px.scatter(p3, x='Driver ID', y='time', color='fatigue', range_y=[
    0, 25], opacity=.8)
'''

app.layout = html.Div([
    html.H2(children='Fatigue Report'),
    dcc.Dropdown(
        id='province',
        options=[
            {'label': 'ALL', 'value': 'ALL'},
            {'label': 'A', 'value': 'A'},
            {'label': 'B', 'value': 'B'},
            {'label': 'C', 'value': 'C'}
        ],
        value='ALL',
        multi=True
    ),
    html.Div(children=[
        dcc.Graph(
            id='city')
    ])
])


@ app.callback(Output('city', 'figure'),
               Input('province', 'value'))
def province2city(province):
    if 'ALL' in province:
        gif = px.scatter(da, x='Driver ID', y='time', color='fatigue', range_y=[
            0, 25], opacity=.8)
    else:
        tf = []
        for i in range(len(da)):
            tf.append(da['Driver ID'].iloc[i] in province)

        df = da[tf]
        gif = px.scatter(df, x='Driver ID', y='time', color='fatigue', range_y=[
            0, 25], opacity=.8)

    return gif


if __name__ == '__main__':
    app.run_server(debug=True)


# %%
'''
# %%

import dash
import dash_core_components as dcc
import dash_html_components as html
import math
import pandas as pd
import plotly.graph_objs as go
import plotly.express as px
from sklearn.decomposition import PCA
from sklearn import datasets
# from mpl_toolkits.mplot3d import Axes3D
# import matplotlib.pyplot as plt
import pandas as pd
from dash.dependencies import Input, Output


app = dash.Dash()
iris = datasets.load_iris()
data = pd.DataFrame(iris['data'])
y = pd.DataFrame(iris['target'])
aa = pd.concat([data, y], axis=1)
aa.columns = ['x1', 'x2', 'x3', 'x4', 'y']


fig = px.box(aa, x='y', y='x1', notched=True)
app.layout = html.Div(
    [
        html.H1('下拉選擇'),
        html.Br(),
        dcc.Dropdown(
            id='province',
            options=[
                {'label': '選項1', 'value': '選項2'},
                {'label': '選項二', 'value': '選項二'},
                {'label': '選項三', 'value': '選項三'}
            ],
            value='選項2'
        ),
        html.P(id='city'),

        html.H2(children='box plot'),
        dcc.Graph(
            id='box',
            figure=fig
        )

    ]

)


province2city_dict = {
    '選項2': '成都市',
    '選項二': '西安市',
    '選項三': "22"
}


@ app.callback(Output('city', 'children'),
               Input('province', 'value'))
def province2city(province):

    return province2city_dict[province]


if __name__ == '__main__':
    app.run_server(debug=True)

# %%
'''
