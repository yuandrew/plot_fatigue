import math

import dash
import dash_core_components as dcc
import dash_html_components as html
import numpy as np
import pandas as pd
import plotly.graph_objs as go
import plotly_express as px
from dash.dependencies import Input, Output

app = dash.Dash()


id = pd.DataFrame(np.repeat('A', 24))
time = pd.DataFrame(range(1, 25))

fatigue = pd.DataFrame([0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1])
fatigue2 = pd.DataFrame([0, 0, 0, 0, 0, 0, 0, 1, 1, 1,
                        0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1])
fatigue3 = pd.DataFrame([0, 0, 0, 1, 1, 0, 0, 1, 1, 1,
                        1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0])

A = pd.concat([id, time, fatigue], axis=1)
B = pd.concat([pd.DataFrame(np.repeat('B', 24)), time, fatigue2], axis=1)
C = pd.concat([pd.DataFrame(np.repeat('C', 24)), time, fatigue3], axis=1)
da = pd.concat([A, B, C])
da.columns = ['Driver ID', 'time', 'fatigue']
A.columns = ['Driver ID', 'time', 'fatigue']
B.columns = ['Driver ID', 'time', 'fatigue']
C.columns = ['Driver ID', 'time', 'fatigue']


for j in [A, B, C, da]:
    for i in range(0, len(j)):
        if(j['fatigue'].iloc[i] == 1):
            j['fatigue'].iloc[i] = 'warnimg'
        else:
            j['fatigue'].iloc[i] = 'normal'


gifall = px.scatter(da, x='Driver ID', y='time', color='fatigue', range_y=[
    0, 25], opacity=.8)

gifa = px.scatter(A, x='Driver ID', y='time', color='fatigue', range_y=[
                  0, 25], opacity=.8)
gifb = px.scatter(B, x='Driver ID', y='time', color='fatigue', range_y=[
                  0, 25], opacity=.8)
gifc = px.scatter(C, x='Driver ID', y='time', color='fatigue', range_y=[
                  0, 25], opacity=.8)

all_gif = {
    'A': gifa,
    'B': gifb,
    'C': gifc,
    'ALL': gifall
}


app.layout = html.Div([
    html.H2(children='Fatigue Report'),


    html.Div(children=[
        dcc.Slider(
            id='province',
            min=0,
            max=100,
            value=0,
            marks={
                33: 'A',
                66: 'b',
                100: 'C',
                0: 'ALL'
            },
            step=None,

        ),

        dcc.Graph(
            id='city',
            animate=False)
    ])
])


@ app.callback(Output('city', 'figure'),
               Input('province', 'value'))
def province2city(selected_driver):
    if selected_driver == 33:
        gif = gifa
    elif selected_driver == 66:
        gif = gifb
    elif selected_driver == 100:
        gif = gifc
    else:
        gif = gifall
        '''
    if 'ALL' in selected_driver:
        gif = px.scatter(da, x='Driver ID', y='time', color='fatigue', range_y=[
            0, 25], opacity=.8)
    else:
        tf = []
        for i in range(len(da)):
            tf.append(da['Driver ID'].iloc[i] in selected_driver)

        df = da[tf]
        gif = px.scatter(df, x='Driver ID', y='time', color='fatigue', range_y=[
            0, 25], opacity=.8)
'''
    return gif


#    return selected_driver


if __name__ == '__main__':
    app.run_server(debug=True)


'''
            marks={
                'A': {'label': 'A', 'value': 'A'},
                'B': {'label': 'B', 'value': 'B'},
                'C': {'label': 'C', 'value': 'C'},
                'ALL':  {'label': 'All', 'value': 'ALL'}
            },
            '''
